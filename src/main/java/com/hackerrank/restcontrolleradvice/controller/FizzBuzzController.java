package com.hackerrank.restcontrolleradvice.controller;

import com.hackerrank.restcontrolleradvice.dto.BuzzException;
import com.hackerrank.restcontrolleradvice.dto.FizzBuzzException;
import com.hackerrank.restcontrolleradvice.dto.FizzBuzzResponse;
import com.hackerrank.restcontrolleradvice.dto.FizzException;
import com.hackerrank.restcontrolleradvice.enums.FizzBuzzEnum;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FizzBuzzController {

  @RequestMapping(value = "/controller_advice/{code}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<FizzBuzzResponse> getFizzBuzzResponse(@PathVariable("code") String code)
          throws FizzException, BuzzException, FizzBuzzException {
    if (FizzBuzzEnum.FIZZ.getValue().equals(code)) {
      String message = "Fizz Exception has been thrown";
      String reason = "Internal Server Error";
      throw new FizzException(message,reason);
    } else if (FizzBuzzEnum.BUZZ.getValue().equals(code)) {
      String message = "Buzz Exception has been thrown";
      String reason = "Bad Request";
      throw new BuzzException(message,reason);
    } else if (FizzBuzzEnum.FIZZBUZZ.getValue().equals(code)) {
      String message = "FizzBuzz Exception has been thrown";
      String reason = "Insufficient Storage";
      throw new FizzBuzzException(message,reason);
    }
    String message = "Successfully completed fizzbuzz test";
    int codeData = 200;
    return new ResponseEntity<>(new FizzBuzzResponse(message,codeData), HttpStatus.OK);

  }
}
