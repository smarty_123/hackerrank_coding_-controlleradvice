package com.hackerrank.restcontrolleradvice.exception;

import com.hackerrank.restcontrolleradvice.dto.BuzzException;
import com.hackerrank.restcontrolleradvice.dto.FizzBuzzException;
import com.hackerrank.restcontrolleradvice.dto.FizzException;
import com.hackerrank.restcontrolleradvice.dto.GlobalError;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class FizzBuzzExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(BuzzException.class)
  public ResponseEntity<Object> handleBuzzException(BuzzException buzz,WebRequest request)
  {
    String message = "Buzz Exception has been thrown";
    String reason = "Bad Request";
    GlobalError globalError = new GlobalError(buzz.getMessage(), buzz.getErrorReason());
    return new ResponseEntity<>(globalError,HttpStatus.BAD_REQUEST);
  }
   @ExceptionHandler(FizzBuzzException.class)
  public ResponseEntity<Object> handleFizzBuzzException(FizzBuzzException buzz,WebRequest request)
  {String message = "FizzBuzz Exception has been thrown";
    String reason = "Insufficient Storage";
    GlobalError globalError = new GlobalError(buzz.getMessage(), buzz.getErrorReason());
    return new ResponseEntity<>(globalError,HttpStatus.INSUFFICIENT_STORAGE);
  }
   @ExceptionHandler(FizzException.class)
  public ResponseEntity<Object> handleFizzException(FizzException buzz,WebRequest request)
  {String message = "Fizz Exception has been thrown";
   String reason = "Internal Server Error";
   GlobalError globalError = new GlobalError(buzz.getMessage(), buzz.getErrorReason());
    return new ResponseEntity<>(globalError,HttpStatus.INTERNAL_SERVER_ERROR);
  }

  //TODO:: implement handler methods for FizzException, BuzzException and FizzBuzzException
}
